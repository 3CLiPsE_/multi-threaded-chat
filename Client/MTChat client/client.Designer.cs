﻿namespace MTChat_client
{
    partial class client
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.username = new System.Windows.Forms.Label();
            this.msgInput = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.send = new System.Windows.Forms.Button();
            this.messanger = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // username
            // 
            this.username.AutoSize = true;
            this.username.Location = new System.Drawing.Point(12, 9);
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(0, 17);
            this.username.TabIndex = 0;
            // 
            // msgInput
            // 
            this.msgInput.Location = new System.Drawing.Point(197, 507);
            this.msgInput.Name = "msgInput";
            this.msgInput.Size = new System.Drawing.Size(623, 22);
            this.msgInput.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(61, 507);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Enter Message:";
            // 
            // send
            // 
            this.send.Location = new System.Drawing.Point(826, 507);
            this.send.Name = "send";
            this.send.Size = new System.Drawing.Size(91, 22);
            this.send.TabIndex = 4;
            this.send.Text = "Send";
            this.send.UseVisualStyleBackColor = true;
            this.send.Click += new System.EventHandler(this.send_Click);
            // 
            // messanger
            // 
            this.messanger.Location = new System.Drawing.Point(197, 12);
            this.messanger.Name = "messanger";
            this.messanger.ReadOnly = true;
            this.messanger.Size = new System.Drawing.Size(720, 489);
            this.messanger.TabIndex = 5;
            this.messanger.Text = "";
            // 
            // client
            // 
            this.AcceptButton = this.send;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(929, 541);
            this.Controls.Add(this.messanger);
            this.Controls.Add(this.send);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.msgInput);
            this.Controls.Add(this.username);
            this.Name = "client";
            this.Text = "client";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label username;
        private System.Windows.Forms.TextBox msgInput;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button send;
        private System.Windows.Forms.RichTextBox messanger;
    }
}