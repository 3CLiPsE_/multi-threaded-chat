﻿using System;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Configuration;
using System.Threading;
using System.Drawing;
using System.Linq;


namespace MTChat_client
{
    public partial class client : Form
    {
        enum Codes : byte { MESSAGE = 1, ADMIN, KICK, MUTE, PRIVATE, USERNAME };
        TcpClient sock;
        string IP = ConfigurationManager.AppSettings.Get("IP");
        int PORT = int.Parse(ConfigurationManager.AppSettings.Get("port"));
        // get the port and IP from config file

        public client()
        {
            InitializeComponent();
            FormClosing += new FormClosingEventHandler(client_FormClosing);

            sock = new TcpClient();
            sock.Connect(IP, PORT);

            client_login login = new client_login();
            login.ShowDialog();
            Username = login.textBox1.Text;
            LoginFinished();
            login.Dispose();

            Thread thread = new Thread(() => getMessagesFromServer());
            thread.Start();     // handle incoming messages in the background
        }
        public string Username
        {
            get
            {
                return username.Text.Substring(username.Text.IndexOf(" ") + 1);
            }
            set
            {
                username.Text = "User: " + value;
            }
        }
        void sendMessage(string message, Codes code)
        {
            NetworkStream serverStream = sock.GetStream();
            byte[] toSend = System.Text.Encoding.ASCII.GetBytes(message);
            // encode the message's length
            byte[] msgLen = System.Text.Encoding.ASCII.GetBytes(toSend.Length.ToString().PadLeft(10, '0'));

            if (code != 0)
            {
                serverStream.Write(new byte[] { (byte)code }, 0, 1);
                serverStream.Flush();
            }
            serverStream.Write(msgLen, 0, msgLen.Length);
            serverStream.Flush();
            serverStream.Write(toSend, 0, toSend.Length);
            serverStream.Flush();
        }
        string getMessage()
        {
            int msgLen = int.Parse(getMessage(10));     // get the message's length

            return getMessage(msgLen);
        }
        string getMessage(int bytes)
        {
            NetworkStream serverStream = sock.GetStream();

            byte[] inStream = new byte[bytes];
            serverStream.Read(inStream, 0, bytes);

            return System.Text.Encoding.ASCII.GetString(inStream);
        }
        public void getMessagesFromServer()
        {
            while (true)
            {
                string sender = getMessage();
                string time = getMessage(8);
                string msg = getMessage();
                if (sender[0] == '@')
                    messanger.AppendText(String.Format("{0}\t{1}: {2}\n", time, sender, msg), Color.RoyalBlue);
                else if (sender[0] == '!')
                    messanger.AppendText(String.Format("{0}\t{1}: {2}\n", time, sender, msg), Color.Chocolate);
                else if (sender == "SERVER_ERROR")
                    MessageBox.Show(String.Format("{0} - {1}", time, msg), sender, MessageBoxButtons.OK);
                else if (sender == "SERVER_INFO")
                    messanger.AppendText(String.Format("{0}\t{1}: {2}\n", time, sender, msg), Color.Green);
                else
                    messanger.AppendText(String.Format("{0}\t{1}: {2}\n", time, sender, msg));
            }
        }
        void LoginFinished()
        {
            sendMessage(Username, Codes.USERNAME);  // notify server of our username
            msgInput.Focus();
        }

        private void send_Click(object sender, EventArgs e)
        {
            string[] words = msgInput.Text.Split(' ');
            if (words[0] == "!kick" && words.Length == 2)
                sendMessage(words[1], Codes.KICK);
            else if (words[0] == "!assign" && words.Length == 2)
                sendMessage(words[1], Codes.ADMIN);
            else if (words[0] == "!mute" && words.Length == 2)
                sendMessage(words[1], Codes.MUTE);
            else if (words[0] == "!private" && words.Length >= 3)
            {
                sendMessage(words[1], Codes.PRIVATE);
                sendMessage(String.Join(" ", words.Skip(2)), 0);
            }
            else
            {
                sendMessage(msgInput.Text, Codes.MESSAGE);
                if (msgInput.Text == "!exit")
                {
                    sock.Close();
                    Close();
                }
            }
            msgInput.Clear();
            msgInput.Focus();
        }
        private void client_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (sock.Connected)
            {
                sendMessage("!exit", Codes.MESSAGE);
                sock.Close();
            }
        }
    }

    public static class RichTextBoxExtensions
    {
        public static void AppendText(this RichTextBox box, string text, Color color)
        {
            box.SelectionStart = box.TextLength;
            box.SelectionLength = 0;

            box.SelectionColor = color;
            box.AppendText(text);
            box.SelectionColor = box.ForeColor;
        }
    }
}
