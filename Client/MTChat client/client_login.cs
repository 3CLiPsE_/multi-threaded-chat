﻿using System;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace MTChat_client
{
    public partial class client_login : Form
    {
        public client_login()
        {
            InitializeComponent();
        }

        private bool ValidUsername(string username)
        {
            return Regex.IsMatch(username, @"^[a-zA-Z0-9_]+$");
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (!ValidUsername(textBox1.Text))
                MessageBox.Show("Username is invalid.", "ERROR", MessageBoxButtons.OK);
            else
                Close();
        }
    }
}
