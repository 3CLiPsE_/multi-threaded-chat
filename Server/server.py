import _thread
import socket
import datetime
import queue
import json
import re

MAX_LENGTH_LEN = 10
TIMEOUT = 5*60  # 5 minutes
codes = {"MESSAGE": 1, "ADMIN": 2, "KICK": 3, "MUTE": 4, "PRIVATE": 5, "USERNAME": 6}
JSON_CONFIG = "config.json"


class Message:
    def __init__(self, sender, message, time_accepted):
        self.sender = sender
        self.message = message
        self.time = time_accepted


class Client:
    def __init__(self, sock, username):
        self.username = username
        self.sock = sock
        self.is_admin = False
        self.is_muted = False


class Server:
    def __init__(self, port):
        self.__sock = socket.socket()
        self.__clients = []  # all clients
        self.__msg = queue.Queue()  # queue of all messages
        self.port = port
        with open(JSON_CONFIG) as config_file:
            self.__admins = json.load(config_file)

    @staticmethod
    def __get_time():
        """
        Gets current time parsed as 'HH:MM:SS'
        :return: time
        :rtype: str
        """
        return datetime.datetime.now().strftime("%H:%M:%S")

    @staticmethod
    def __get_msg(sock):
        """
        Gets message from socket according to protocol
        :param sock: client socket
        :type sock: socket.socket
        :return: client's message
        :rtype: str
        """
        code = int.from_bytes(sock.recv(1), byteorder="big")
        length = int(sock.recv(MAX_LENGTH_LEN).decode())  # first get length
        msg = sock.recv(length).decode()
        if code != codes["PRIVATE"]:
            return code, msg
        sender = msg
        msg = sock.recv(int(sock.recv(MAX_LENGTH_LEN).decode())).decode()
        return code, sender, msg

    @staticmethod
    def __send_msg(client, msg):
        """
        Sends a message to the client according to protocol
        :param client: client
        :type client: Client
        :param msg: message to send
        :type msg: Message
        :return: None
        """
        client.sock.sendall(str(len(msg.sender.username)).zfill(MAX_LENGTH_LEN).encode())
        client.sock.sendall(msg.sender.username.encode())
        client.sock.sendall(msg.time.encode())  # always 8 bytes
        client.sock.sendall(str(len(msg.message)).zfill(MAX_LENGTH_LEN).encode())
        client.sock.sendall(msg.message.encode())

    @staticmethod
    def __send_error(client, error_msg, time):
        """
        Send server error message to a client
        :param client: client to send error to
        :type client: Client
        :param error_msg: message to send
        :type error_msg: str
        :param time: time of message that made error occur
        :type time: str
        :return: None
        """
        Server.__send_msg(client, Message(Client(None, "SERVER_ERROR"), error_msg, time))

    def commit_json(self):
        """
        Dumps the admins list into the config file.
        :return: None
        """
        with open(JSON_CONFIG, 'w') as config_file:
            json.dump(self.__admins, config_file)

    def __is_username_valid(self, username):
        """
        Checks if the username given is valid
        :param username: to check
        :type username: str
        :return: is username valid or not
        :rtype: bool
        """
        if len(re.findall("^[a-zA-Z0-9_]+$", username)) == 0:
            return username not in [x.username for x in self.__clients]
        return False

    def __does_client_exist(self, username):
        """
        Checks if a client exists
        :param username: client's name
        :type username: str
        :return: does client exist
        :rtype: bool
        """
        return username in [client.username for client in self.__clients]

    def __get_client_by_name(self, username):
        """
        Returns the right client from the list
        :param username: username of wanted client
        :type username: str
        :return: client wanted
        :rtype: Client
        """
        for client in self.__clients:
            if client.username == username:
                return client

    def __remove_client(self, client):
        """
        Removes client from the server.
        :param client: client to remove
        :type client: Client
        :return: None
        """
        self.__clients.remove(client)
        client.sock.close()

    def __update_client(self, client_old, client):
        """
        Updates the client in the clients list
        :param client: client to insert
        :type client: Client
        :param client_old: client to remove
        :type client_old: Client
        :return: None
        """
        self.__clients.remove(client_old)
        self.__clients.append(client)

    def __check_admin(self, client):
        """
        Checks if the client deserves admin role and gives it to him
        :param client: client to check
        :type client: Client
        :return: None
        """
        if client.username in self.__admins["ADMINS"]:  # assign admin role
            self.__clients.remove(client)
            client.username = f"@{client.username}"
            client.is_admin = True
            self.__clients.append(client)

    def __check_muted(self, to_check):
        """
        Checks if a client should be muted, and mutes him if he does.
        :param to_check: client to check
        :type to_check: Client
        :return: None
        """
        for client in self.__clients:
            if client.username == to_check.username:
                to_check.is_muted = client.is_muted
                return

    def handle_messages(self):
        """
        Handles all messages gotten from clients
        :return: None
        """
        while True:
            message = self.__msg.get()  # get first message form queue
            for client in self.__clients:
                if client is not message.sender:
                    Server.__send_msg(client, message)
            self.__msg.task_done()  # message was taken care of

    def handle_client(self, client):
        """
        Handles the client (gets messages from client)
        :param client: the client to be handled
        :type client: Client
        :return: None
        """
        if not self.__is_username_valid(client.username):
            pass
        client.sock.settimeout(TIMEOUT)
        while True:
            self.__check_admin(client)
            self.__check_muted(client)
            try:
                msg = tuple(Server.__get_msg(client.sock))
            except TimeoutError:
                self.__remove_client(client)
                return
            except socket.error:
                client.sock.close()
                return
            code = msg[0]
            msg = msg[1:]
            time = self.__get_time()
            if msg[0] == "!exit":
                self.__remove_client(client)
                print(f"Disconnected from client {client.username}")
                self.__msg.put_nowait(Message(client, "--- Disconnected ---", time))
                return
            if code == codes["MESSAGE"]:
                if msg[0] == "!admins":
                    self.__send_msg(client, Message(Client(None, "SERVER_INFO"), str(self.__admins["ADMINS"]), time))
                elif client.is_muted:
                    self.__send_error(client, "Muted users can't send messages.", time)
                else:
                    self.__msg.put_nowait(Message(client, msg[0], time))  # put message on queue to be handled
            elif code == codes["KICK"]:
                if not client.is_admin:  # if not admin
                    self.__send_error(client, "Only an admin can kick users.", time)
                else:
                    if not self.__does_client_exist(msg[0]):
                        self.__send_error(client, f"No user {msg[0]} exists.", time)
                    else:
                        self.__remove_client(self.__get_client_by_name(msg[0]))
                        self.__msg.put_nowait(Message(client, f"--- {msg[0]} has been kicked. ---", time))
            elif code == codes["ADMIN"]:
                if not client.is_admin:
                    self.__send_error(client, "Only an admin can assign other admins.", time)
                else:
                    if not self.__does_client_exist(msg[0]):
                        self.__send_error(client, f"No user {msg[0]} exists.", time)
                    else:
                        self.__admins["ADMINS"].append(msg[0])
                        self.commit_json()
                        self.__msg.put_nowait(Message(client, f"--- {msg[0]} has been assigned admin roles. ---", time))
            elif code == codes["MUTE"]:
                if not client.is_admin:  # if not admin
                    self.__send_error(client, "Only an admin can mute users.", time)
                else:
                    if not self.__does_client_exist(msg[0]):
                        self.__send_error(client, f"No user {msg[0]} exists.", time)
                    else:
                        self.__get_client_by_name(msg[0]).is_muted = True
                        self.__msg.put_nowait(Message(client, f"--- {msg[0]} has been muted. ---", time))
            elif code == codes["PRIVATE"]:
                if not self.__does_client_exist(msg[0]):
                    self.__send_error(client, f"No user {msg[0]} exists.", time)
                else:
                    self.__send_msg(self.__get_client_by_name(msg[0]), Message(Client(client.sock, '!'+client.username), msg[1], time))

    def run(self):
        """
        Starts the server's operation
        :return: None
        """
        self.__sock.bind(("0.0.0.0", self.port))
        self.__sock.listen(5)
        _thread.start_new_thread(Server.handle_messages, (self,))  # start new thread for handling messages
        while True:
            client_sock = self.__sock.accept()[0]
            code, username = self.__get_msg(client_sock)
            if code != codes["USERNAME"]:
                client_sock.close()
                continue
            client = Client(client_sock, username)
            self.__clients.append(client)
            _thread.start_new_thread(Server.handle_client,
                                     (self, client))  # start new thread for each client to be handled
            print(f"Started handling client {client.username}")
